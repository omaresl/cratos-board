#include "app_HX711.h"

#define _XTAL_FREQ 64000000
#define N_SAMPLES 10u
unsigned long rul_LastMeasurement;
unsigned long rul_AvgMEasValue;
unsigned long raul_Measures[N_SAMPLES];
unsigned char rub_MeasIndex;
unsigned char rub_HX711_DataReady;

/*! \brief This functions initializes the HX711 module
 */
void app_HX711_Init(void) {
    /* Init Data */
    rul_LastMeasurement = 0;
    /* Power Down */
    APP_HX711_SET_PD_SCK_PIN;
    __delay_us(100);
    APP_HX711_CLEAR_PD_SCK_PIN;
    rub_MeasIndex = 0;

    rub_HX711_DataReady = 0;
}

/*! \brief This functions gets the measure from HX711 module
 */
void app_HX711_StartMeasure(void) {
    static unsigned long lul_TimeOut;
    static unsigned long lul_Return;

    /* Restart Timeout Counter */
    lul_TimeOut = 0u;
    /* Clear Return Temp Variable */
    lul_Return = 0x00000000u;

    /*Wait for data ready*/
    if (0x00 != rub_HX711_DataReady) {
        for (unsigned char lub_i = 0u; lub_i < APP_HX711_NBITS; lub_i++) {
            /* Set SCK for new bit*/
            APP_HX711_SET_PD_SCK_PIN; // No more than 50us
            //wait 100ns
            __delay_us(15);
            lul_Return = lul_Return << 1u;
            /* Set SCK for new bit*/
            APP_HX711_CLEAR_PD_SCK_PIN;
            __delay_us(15);
            if (APP_HX711_GET_DOUT_PIN) {
                lul_Return++;
            }
        }

        //Gain cfg clks
        for (unsigned char lub_j = 0u; lub_j < APP_HX711_GAIN; lub_j++) {
            /* Set SCK for new bit*/
            APP_HX711_SET_PD_SCK_PIN; // No more than 50us
            __delay_us(15);
            //wait 100ns
            /* Set SCK for new bit*/
            APP_HX711_CLEAR_PD_SCK_PIN;
            __delay_us(15);
        }

        rul_LastMeasurement = lul_Return;
        raul_Measures[rub_MeasIndex] = rul_LastMeasurement;

        rul_LastMeasurement = raul_Measures[rub_MeasIndex];
        for (unsigned char lub_k = 1; lub_k < N_SAMPLES; lub_k++) {
            rul_LastMeasurement += raul_Measures[lub_k];
            rul_LastMeasurement /= 2;
        }
        if(rub_MeasIndex < (N_SAMPLES - 1u))
        {
            rub_MeasIndex++;
        }
        else
        {
            rub_MeasIndex = 0u;
        }
        rul_AvgMEasValue = rul_LastMeasurement;
        rub_HX711_DataReady = 0u;
    } else {
        /* Do nothing */
    }
}

/*! \brief This functions returns the last measure from HX711 module
 */
unsigned long app_HX711_GetMeasure(void) {
    return rul_AvgMEasValue;
}