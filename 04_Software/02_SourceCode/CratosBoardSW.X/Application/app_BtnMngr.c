/*
 * app_BtnMngr.c
 *
 * Created: 22/11/2019 10:48:27 a.m.
 *  Author: uidj2522
 */
#include "app_BtnMngr.h"

T_BTN_State re_BtnState[APP_BTNMNGR_N_BUTTONS] = {NOT_PRESSED, NOT_PRESSED};
unsigned char raub_NewDebFlag[APP_BTNMNGR_N_BUTTONS] = {0x00, 0x00};
unsigned short ruw_BtnDebounceCounter[APP_BTNMNGR_N_BUTTONS];
unsigned char rub_BtnMngrTaskFlag = 0u;

static void app_BtnMngr_DebBtn(void);
static void app_BtnMngr_DebBtn1(void);
static void app_BtnMngr_DebBtn2(void);

static unsigned char rub_LastBtnValue;
static unsigned char rub_ActualBtnValue;
static unsigned char rub_NewDebBtnFlag;
static unsigned char rub_LastDebBtnValue;
static unsigned char rub_DebBtnValue;
static T_BTN_State rub_DebBtnState;
static unsigned int ruw_BtnDebCounter;


/************************************************************************/
/* Name: app_BtnMngr_Init                                               */
/* Description: TBD                       								*/
/* Parameters: None                                                     */
/* Return: None                                                         */

/************************************************************************/
void app_BtnMngr_Init(void) {
    APP_BTNMNGR_BTN_1_STATE = NOT_PRESSED;
    APP_BTNMNGR_BTN_2_STATE = NOT_PRESSED;

    //Clear counters
    APP_BTNMNGR_BTN_1_DCNTR = 0u;
    APP_BTNMNGR_BTN_2_DCNTR = 0u;

#define BUTTON_MASK 0x03u
    rub_LastBtnValue = BUTTON_MASK;
    rub_ActualBtnValue = BUTTON_MASK;
    rub_NewDebBtnFlag = 0x00u;
    rub_LastDebBtnValue = BUTTON_MASK;
    rub_DebBtnValue = BUTTON_MASK;
    rub_DebBtnState = NOT_PRESSED;
    ruw_BtnDebCounter = 0u;
}

/************************************************************************/
/* Name: app_BtnMngr_Task                                               */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */

/************************************************************************/
void app_BtnMngr_Task(void) {
    if (rub_BtnMngrTaskFlag == 0x01u) {
        rub_BtnMngrTaskFlag = 0x00;
        //        app_BtnMngr_DebBtn1();
        //        app_BtnMngr_DebBtn2();
        app_BtnMngr_DebBtn();
    } else {
    }
}

static void app_BtnMngr_DebBtn(void) {
    rub_ActualBtnValue = APP_BTNMNGR_READ_BTN_1 | (APP_BTNMNGR_READ_BTN_2 << 1);
    /* Check previous and actual values*/
    if (rub_LastBtnValue != rub_ActualBtnValue) {
        /* Reset Debounce Counter */
        ruw_BtnDebCounter = 0u;
        rub_LastBtnValue = rub_ActualBtnValue;
        rub_DebBtnState = NOT_PRESSED;
    } else {
        if (ruw_BtnDebCounter >= APP_BTNMNGR_LONGPRESS_DEBTIME && rub_DebBtnState != LONG_PRESS) {
            rub_DebBtnValue = rub_ActualBtnValue;
            rub_DebBtnState = LONG_PRESS;
        } else if (ruw_BtnDebCounter >= APP_BTNMNGR_SHORTPRESS_DEBTIME && rub_DebBtnState != SHORT_PRESS) {
            rub_DebBtnValue = rub_ActualBtnValue;
            rub_DebBtnState = SHORT_PRESS;
        } else {
            /* Do nothing */
        }

        if (rub_LastDebBtnValue != rub_DebBtnValue) {
            rub_LastDebBtnValue = rub_DebBtnValue;
            rub_NewDebBtnFlag = 0x01;
        } else {
            /* Do nothing */
        }

        if (ruw_BtnDebCounter >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME) {
            //Don't increment
        } else {
            ruw_BtnDebCounter++;
        }
    }
}

unsigned char app_BtnMngr_GetDbnBtn(void) {
    return rub_DebBtnValue;
}

unsigned char app_BtnMngr_IsNewDebBtn(void) {
    return rub_NewDebBtnFlag;
}

void app_BtnMngr_ClearNewDebBtnFlag(void) {
    rub_NewDebBtnFlag = 0;
}

T_BTN_State app_BtnMngr_GetDbnBtnState(void) {
    return rub_DebBtnState;
}

/************************************************************************/
/* Name: app_BtnMngr_DebBtn1                                            */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */

/************************************************************************/
static void app_BtnMngr_DebBtn1(void) {
    //Check input
    if (0x00 == APP_BTNMNGR_READ_BTN_1) {//Button Pressed
        //Check debounce
        if (APP_BTNMNGR_BTN_1_DCNTR >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME) {
            APP_BTNMNGR_BTN_1_STATE = VERY_LONG_PRESS;
            APP_BTNMNGR_BTN_1_NEWFLAG = 0x01u;
        } else if (APP_BTNMNGR_BTN_1_DCNTR >= APP_BTNMNGR_LONGPRESS_DEBTIME) {
            APP_BTNMNGR_BTN_1_STATE = LONG_PRESS;
            APP_BTNMNGR_BTN_1_NEWFLAG = 0x01u;
        } else if (APP_BTNMNGR_BTN_1_DCNTR >= APP_BTNMNGR_SHORTPRESS_DEBTIME) {
            APP_BTNMNGR_BTN_1_STATE = SHORT_PRESS;
            APP_BTNMNGR_BTN_1_NEWFLAG = 0x01u;
        } else {
            APP_BTNMNGR_BTN_1_STATE = NOT_PRESSED;
            APP_BTNMNGR_BTN_1_NEWFLAG = 0x00u;
        }

        if (APP_BTNMNGR_BTN_1_DCNTR >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME) {
            //Don't increment
        } else {
            APP_BTNMNGR_BTN_1_DCNTR++;
        }
    } else {
        APP_BTNMNGR_BTN_1_STATE = NOT_PRESSED;
        APP_BTNMNGR_BTN_1_DCNTR = 0;
        APP_BTNMNGR_BTN_1_NEWFLAG = 0x00u;
    }
}

/************************************************************************/
/* Name: app_BtnMngr_DebBtn2                                            */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */

/************************************************************************/
static void app_BtnMngr_DebBtn2(void) {
    //Check input
    if (0x00 == APP_BTNMNGR_READ_BTN_2) {//Button Pressed
        //Check debounce
        if (APP_BTNMNGR_BTN_2_DCNTR >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME) {
            APP_BTNMNGR_BTN_2_STATE = VERY_LONG_PRESS;
            APP_BTNMNGR_BTN_2_NEWFLAG = 0x01u;
        } else if (APP_BTNMNGR_BTN_2_DCNTR >= APP_BTNMNGR_LONGPRESS_DEBTIME) {
            APP_BTNMNGR_BTN_2_STATE = LONG_PRESS;
            APP_BTNMNGR_BTN_2_NEWFLAG = 0x01u;
        } else if (APP_BTNMNGR_BTN_2_DCNTR >= APP_BTNMNGR_SHORTPRESS_DEBTIME) {
            APP_BTNMNGR_BTN_2_STATE = SHORT_PRESS;
            APP_BTNMNGR_BTN_2_NEWFLAG = 0x01u;
        } else {
            APP_BTNMNGR_BTN_2_STATE = NOT_PRESSED;
            APP_BTNMNGR_BTN_2_NEWFLAG = 0x00u;
        }

        if (APP_BTNMNGR_BTN_2_DCNTR >= APP_BTNMNGR_VERYLONGPRESS_DEBTIME) {
            //Don't increment
        } else {
            APP_BTNMNGR_BTN_2_DCNTR++;
        }
    } else {
        APP_BTNMNGR_BTN_2_STATE = NOT_PRESSED;
        APP_BTNMNGR_BTN_2_NEWFLAG = 0x00u;
        APP_BTNMNGR_BTN_2_DCNTR = 0;
    }
}

/************************************************************************/
/* Name: app_BtnMngr_GetBtn1State                                       */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */

/************************************************************************/
T_BTN_State app_BtnMngr_GetBtn1State(void) {
    return APP_BTNMNGR_BTN_1_STATE;
}

/************************************************************************/
/* Name: app_BtnMngr_GetBtn1State                                       */
/* Description: TBD                     								*/
/* Parameters: None                                                     */
/* Return: None                                                         */

/************************************************************************/
T_BTN_State app_BtnMngr_GetBtn2State(void) {
    return APP_BTNMNGR_BTN_2_STATE;
}