/*
 * app_BtnMngr.h
 *
 * Created: 22/11/2019 10:48:11 a.m.
 *  Author: uidj2522
 */ 


#ifndef APP_BTNMNGR_H_
#define APP_BTNMNGR_H_

#include "../mcc_generated_files/pin_manager.h"

typedef enum
{
	NOT_PRESSED,
	SHORT_PRESS,
	LONG_PRESS,
	VERY_LONG_PRESS
}T_BTN_State;

#define APP_BTNMNGR_BTN_1_INDEX	0u
#define APP_BTNMNGR_BTN_2_INDEX	1u

#define APP_BTNMNGR_BTN_1_STATE	re_BtnState[APP_BTNMNGR_BTN_1_INDEX]
#define APP_BTNMNGR_BTN_2_STATE	re_BtnState[APP_BTNMNGR_BTN_2_INDEX]

#define APP_BTNMNGR_BTN_1_NEWFLAG	raub_NewDebFlag[APP_BTNMNGR_BTN_1_INDEX]
#define APP_BTNMNGR_BTN_2_NEWFLAG	raub_NewDebFlag[APP_BTNMNGR_BTN_2_INDEX]

#define APP_BTNMNGR_BTN_1_DCNTR	ruw_BtnDebounceCounter[APP_BTNMNGR_BTN_1_INDEX]
#define APP_BTNMNGR_BTN_2_DCNTR	ruw_BtnDebounceCounter[APP_BTNMNGR_BTN_2_INDEX]

#define APP_BTNMNGR_N_BUTTONS	2u

#define APP_BTNMNGR_READ_BTN_1	IO_RA0_GetValue()
#define APP_BTNMNGR_READ_BTN_2	IO_RA1_GetValue()

#define APP_BTNMNGR_SHORTPRESS_DEBTIME		5		//10 ticks
#define APP_BTNMNGR_LONGPRESS_DEBTIME		100		//100 ticks
#define APP_BTNMNGR_VERYLONGPRESS_DEBTIME	500		//5000 ticks

extern unsigned char	rub_BtnMngrTaskFlag;

/* Public Functions */
extern void app_BtnMngr_Init(void);
extern void app_BtnMngr_Task(void);
T_BTN_State app_BtnMngr_GetBtn1State(void);
T_BTN_State app_BtnMngr_GetBtn2State(void);
extern unsigned char app_BtnMngr_GetDbnBtn(void);
extern unsigned char app_BtnMngr_IsNewDebBtn(void);
extern T_BTN_State app_BtnMngr_GetDbnBtnState(void);
extern void app_BtnMngr_ClearNewDebBtnFlag(void);

#endif /* APP_BTNMNGR_H_ */