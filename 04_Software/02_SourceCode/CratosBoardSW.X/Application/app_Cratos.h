/* 
 * File:   app_Cratos.h
 * Author: uidj2522
 *
 * Created on 10 de diciembre de 2019, 11:42 AM
 */

#ifndef APP_CRATOS_H
#define	APP_CRATOS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "app_BtnMngr.h"
#include "app_Threshold.h"
#include "app_Relay.h"
#include "app_Display.h"
#include "i2c_LCD.h"

    /* Thresholds */
#define APP_CRATOS_THRESHOLD_INIT           app_Threshold_Init()
#define APP_CRATOS_EVALUATEMAX_MEAS(meas)   app_Threshold_EvaluateMax(meas)
#define APP_CRATOS_EVALUATEMIN_MEAS(meas)   app_Threshold_EvaluateMin(meas)
#define APP_CRATOS_INCREASE_THRESHOLD_MAX   app_Threshold_IncreaseMax()
#define APP_CRATOS_DECREASE_THRESHOLD_MAX   app_Threshold_DecreaseMax()
#define APP_CRATOS_INCREASE_THRESHOLD_MIN   app_Threshold_IncreaseMin()
#define APP_CRATOS_DECREASE_THRESHOLD_MIN   app_Threshold_DecreaseMin()

    /* Measure */
#define APP_CRATOS_GET_MEASURE      ((app_HX711_GetMeasure() - app_Threshold_GetCalOffset()) * app_Threshold_GetCalMultiplier())/1000u

    /* Relay */
#define APP_CRATOS_RELAY_ON         app_Relay_On()
#define APP_CRATOS_RELAY_OFF        app_Relay_Off()

    /* Buttons */
#define APP_CRATOS_GET_BTN_STATE    ((app_BtnMngr_IsNewDebBtn() << 7u) | (app_BtnMngr_GetDbnBtnState() << 5u) | (app_BtnMngr_GetDbnBtn()))
#define APP_CRATOS_UP_STATE         ((1u << 7u)|(SHORT_PRESS << 5u)|(0x02u))
#define APP_CRATOS_FAST_UP_STATE    ((0u << 7u)|(LONG_PRESS << 5u) |(0x02u))
#define APP_CRATOS_DOWN_STATE       ((1u << 7u)|(SHORT_PRESS << 5u)|(0x01u))
#define APP_CRATOS_FAST_DOWN_STATE  ((0u << 7u)|(LONG_PRESS << 5u) |(0x01u))
#define APP_CRATOS_CHANGEMODE_STATE ((1u << 7u)|(SHORT_PRESS << 5u)|(0x00u))
#define APP_CRATOS_NOACTION_STATE   ((1u << 7u)|(SHORT_PRESS << 5u)|(0x03u))
#define APP_CRATOS_CAL_STATE        ((0u << 7u)|(LONG_PRESS << 5u) |(0x00u))

    /* Print LCD */
    extern unsigned char counter;
#define APP_CRATOS_PRINT_NORMAL_DATA    app_Display_SetData(rul_Data,0u);app_Display_Normal()
#define APP_CRATOS_PRINT_THRMAX_DATA    app_Display_SetData(app_Threshold_GetMaxValue(),2u);app_Display_THRMAX()
#define APP_CRATOS_PRINT_THRMIN_DATA    app_Display_SetData(app_Threshold_GetMinValue(),1u);app_Display_THRMIN()
#define APP_CRATOS_CLEAR_LCD            i2c_LCD_Clear();

    /* Public Functions */
    extern void app_Cratos_Init(void);
    extern void app_Cratos_Task(void);
    extern void app_Cratos_CheckInputs(void);
    extern void app_Cratos_CfgActions(void);

#ifdef	__cplusplus
}
#endif

#endif	/* APP_CRATOS_H */

