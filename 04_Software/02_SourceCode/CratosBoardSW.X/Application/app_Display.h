/* 
 * File:   app_Display.h
 * Author: uidj2522
 *
 * Created on December 10, 2019, 6:46 PM
 */

#ifndef APP_DISPLAY_H
#define	APP_DISPLAY_H

#ifdef	__cplusplus
extern "C" {
#endif
#include "i2c_LCD.h"

/* Public Functions */
    extern void app_Display_Init(void);
    extern void app_Display_Normal(void);
    extern void app_Display_SetData(unsigned int lub_Data,unsigned char lub_Index);
    extern void app_Display_THRMAX(void);
    extern void app_Display_THRMIN(void);
    
#ifdef	__cplusplus
}
#endif

#endif	/* APP_DISPLAY_H */

