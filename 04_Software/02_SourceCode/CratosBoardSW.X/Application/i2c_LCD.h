/* 
 * File:   i2c_LCD.h
 * Author: OmarSevilla
 *
 * Created on February 2, 2020, 3:25 PM
 */

#ifndef I2C_LCD_H
#define	I2C_LCD_H

#ifdef	__cplusplus
extern "C" {
#endif
#include "../mcc_generated_files/mcc.h"

    extern void i2c_LCD_Init(void);
    extern void i2c_LCD_WriteData(unsigned char* bufer, unsigned char size);
    extern void i2c_LCD_SetBacklight(void);
    extern void i2c_LCD_SetCursor(unsigned char lub_x, unsigned char lub_y);
    extern void i2c_LCD_Print(char *lub_char);
    extern void i2c_LCD_Dec2Dig(unsigned long lub_Dec, unsigned char lub_Digits, unsigned char *lpub_Dest);
    extern void i2c_LCD_Dig2ASCII(unsigned char* lpub_Source, char *lpub_Dest, unsigned char digits);
    extern void i2c_LCD_Clear(void);

#ifdef	__cplusplus
}
#endif

#endif	/* I2C_LCD_H */

