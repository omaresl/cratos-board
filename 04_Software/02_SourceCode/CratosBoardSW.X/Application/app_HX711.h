/* 
 * File:   app_HX711.h
 * Author: uidj2522
 *
 * Created on 9 de diciembre de 2019, 03:47 PM
 */

#ifndef APP_HX711_H
#define	APP_HX711_H

#ifdef	__cplusplus
extern "C" {
#endif
#include "../mcc_generated_files/pin_manager.h"

    /* Pin Interfaces */
#define APP_HX711_CLEAR_PD_SCK_PIN  IO_RB2_SetLow()
#define APP_HX711_SET_PD_SCK_PIN    IO_RB2_SetHigh()
#define APP_HX711_GET_DOUT_PIN      IO_RB1_GetValue()

    /* Configuration */
#define APP_HX711_TIMEOUT   0x0000FFFFu
#define APP_HX711_NBITS     24u

#define APP_HX711_GAIN128   1u
#define APP_HX711_GAIN32    2u
#define APP_HX711_GAIN64    3u
#define APP_HX711_GAIN      APP_HX711_GAIN128
    
    extern unsigned char rub_HX711_DataReady;

    /* Public Functions */
    extern void app_HX711_Init(void);
    extern void app_HX711_StartMeasure(void);
    extern unsigned long app_HX711_GetMeasure(void);
#ifdef	__cplusplus
}
#endif

#endif	/* APP_HX711_H */

