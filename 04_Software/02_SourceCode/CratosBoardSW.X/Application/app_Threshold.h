/* 
 * File:   app_Threshold.h
 * Author: uidj2522
 *
 * Created on December 10, 2019, 4:06 PM
 */

#ifndef APP_THRESHOLD_H
#define	APP_THRESHOLD_H

#ifdef	__cplusplus
extern "C" {
#endif
#include "../mcc_generated_files/memory.h"
    
#define APP_THRESHOLD_MAX_VALUE 99999u
#define APP_THRESHOLD_MIN_VALUE 0u

/* Public Functions */
    extern void app_Threshold_Init(void);
    extern unsigned char app_Threshold_EvaluateMax(unsigned long lul_Measure);
    extern unsigned char app_Threshold_EvaluateMin(unsigned long lul_Measure);
    extern void app_Threshold_IncreaseMax(void);
    extern void app_Threshold_IncreaseMin(void);
    extern void app_Threshold_DecreaseMax(void);
    extern void app_Threshold_DecreaseMin(void);
    extern unsigned long app_Threshold_GetMaxValue(void);
    extern unsigned long app_Threshold_GetMinValue(void);
    extern unsigned long app_Threshold_GetCalOffset(void);
    extern unsigned long app_Threshold_GetCalMultiplier(void);
    extern void app_Threshold_Calibrate(unsigned long lul_Cal);

#ifdef	__cplusplus
}
#endif

#endif	/* APP_THRESHOLD_H */

