/*
 * File:   i2c_LCD.c
 * Author: OmarSevilla
 *
 * Created on February 2, 2020, 3:24 PM
 */

#include "i2c_LCD.h"

void i2c_LCD_Init(void) {
    unsigned char buffer[3u];
    buffer[0] = 0u;
    i2c_LCD_WriteData(&buffer[0], 0x01);

    __delay_ms(1000);

    buffer[0] = 0x30;
    buffer[1] = 0x34;
    buffer[2] = 0x30;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    __delay_ms(5);
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    __delay_ms(5);
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    buffer[0] = 0x20;
    buffer[1] = 0x24;
    buffer[2] = 0x20;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    buffer[0] = 0x80;
    buffer[1] = 0x84;
    buffer[2] = 0x80;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    buffer[0] = 0x00;
    buffer[1] = 0x04;
    buffer[2] = 0x00;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    buffer[0] = 0xC0;
    buffer[1] = 0xC4;
    buffer[2] = 0xC0;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    buffer[0] = 0x00;
    buffer[1] = 0x04;
    buffer[2] = 0x00;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    buffer[0] = 0x10;
    buffer[1] = 0x14;
    buffer[2] = 0x10;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    __delay_ms(2);
    buffer[0] = 0x00;
    buffer[1] = 0x04;
    buffer[2] = 0x00;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    buffer[0] = 0x60;
    buffer[1] = 0x64;
    buffer[2] = 0x60;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
    buffer[0] = 0x00;
    buffer[1] = 0x04;
    buffer[2] = 0x00;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);

    buffer[0] = 0x20;
    buffer[1] = 0x24;
    buffer[2] = 0x20;
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);

}

void i2c_LCD_SetBacklight(void) {
    unsigned char buffer[1u];
    buffer[0] = 0x08u;
    i2c_LCD_WriteData(&buffer[0], 0x01);
}

void i2c_LCD_SetCursor(unsigned char lub_x, unsigned char lub_y) {
    unsigned char buffer[3u];
    buffer[0] = 0x88 | ((lub_y & 0x01) << 6u);
    buffer[1] = 0x8C | ((lub_y & 0x01) << 6u);
    buffer[2] = 0x88 | ((lub_y & 0x01) << 6u);
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);

    buffer[0] = 0x08u | (lub_x << 4u);
    buffer[1] = 0x0Cu | (lub_x << 4u);
    buffer[2] = 0x08u | (lub_x << 4u);
    i2c_LCD_WriteData(&buffer[0], 0x01);
    i2c_LCD_WriteData(&buffer[1], 0x01);
    i2c_LCD_WriteData(&buffer[2], 0x01);
}

void i2c_LCD_Clear(void) {
    i2c_LCD_SetCursor(0, 0);
    i2c_LCD_Print("                ");
    i2c_LCD_SetCursor(0, 1);
    i2c_LCD_Print("                ");
}

void i2c_LCD_Print(char *lub_char) {
    unsigned char buffer[3u];
    unsigned char lub_Index;
    lub_Index = 0u;
    while (0 != lub_char[lub_Index]) {
        buffer[0] = 0x09 | ((lub_char[lub_Index]) & 0xF0);
        buffer[1] = 0x0D | ((lub_char[lub_Index]) & 0xF0);
        buffer[2] = 0x09 | ((lub_char[lub_Index]) & 0xF0);
        i2c_LCD_WriteData(&buffer[0], 0x01);
        i2c_LCD_WriteData(&buffer[1], 0x01);
        i2c_LCD_WriteData(&buffer[2], 0x01);

        buffer[0] = 0x09 | ((lub_char[lub_Index]) << 4u);
        buffer[1] = 0x0D | ((lub_char[lub_Index]) << 4u);
        buffer[2] = 0x09 | ((lub_char[lub_Index]) << 4u);
        i2c_LCD_WriteData(&buffer[0], 0x01);
        i2c_LCD_WriteData(&buffer[1], 0x01);
        i2c_LCD_WriteData(&buffer[2], 0x01);

        lub_Index++;
    }
}

void i2c_LCD_Dec2Dig(unsigned long lub_Dec, unsigned char lub_Digits, unsigned char *lpub_Dest) {
    unsigned char lub_DestIndex;

    lub_DestIndex = 0u;
    switch (lub_Digits) {
        case 5://THOUSANDS
        {
            lpub_Dest[lub_DestIndex] = lub_Dec / 10000u;
            lub_Dec = lub_Dec - (lpub_Dest[lub_DestIndex]*10000);
            lub_DestIndex++;
        }
        case 4://THOUSANDS
        {
            lpub_Dest[lub_DestIndex] = lub_Dec / 1000u;
            lub_Dec = lub_Dec - (lpub_Dest[lub_DestIndex]*1000);
            lub_DestIndex++;
        }
        case 3://HUNDREDS
        {
            lpub_Dest[lub_DestIndex] = lub_Dec / 100u;
            lub_Dec = lub_Dec - (lpub_Dest[lub_DestIndex]*100);
            lub_DestIndex++;
        }
        case 2://TENS
        {
            lpub_Dest[lub_DestIndex] = lub_Dec / 10u;
            lub_Dec = lub_Dec - (lpub_Dest[lub_DestIndex]*10);
            lub_DestIndex++;
        }
        case 1://UNITS
        {
            lpub_Dest[lub_DestIndex] = lub_Dec;
            lub_DestIndex++;
            lpub_Dest[lub_DestIndex] = 0u;
        }
        default:
        {
            //Do Nothing
        }
    }
}

void i2c_LCD_Dig2ASCII(unsigned char* lpub_Source, char *lpub_Dest, unsigned char digits) {
    for (unsigned char lub_i = 0; lub_i < digits; lub_i++) {
        lpub_Dest[lub_i] = (char) lpub_Source[lub_i] + 0x30u;
    }
}

void i2c_LCD_WriteData(unsigned char* buffer, unsigned char size) {
    if (I2C_NOERR == i2c_open(0x27)) {
        i2c_setTimeOut(0x01u);
        i2c_setBuffer(buffer, size);
        i2c_masterWrite();
        while (I2C_BUSY == i2c_close()) {

        }
    } else {
        /* Do nothing */
    }
}
