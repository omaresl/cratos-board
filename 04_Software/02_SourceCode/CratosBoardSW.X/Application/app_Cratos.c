#include "app_Cratos.h"
#include "app_HX711.h"

typedef enum {
    NORMAL,
    THRESHOLD_MAX,
    THRESHOLD_MIN,
    N_MODES
} T_CRATOS_MODES;

typedef enum {
    NO_ACTION,
    CHANGE_MODE_ACTION,
    INCREASE_THRESHOLD,
    DECREASE_THRESHOLD,
    N_ACTIONS
} T_CRATOS_ACTIONS;

T_CRATOS_MODES re_CratosMode;
T_CRATOS_ACTIONS re_CratosAction;
unsigned long rul_Data;

/* Private Functions */
static void app_Cratos_DisplayTask(void);

/* brief! This function initialize ules and data needed for Cratos Functionality
 */
void app_Cratos_Init(void) {
    /* Initialize all peripherials */
    app_Relay_Init();
    app_BtnMngr_Init();
    app_HX711_Init();
    app_Display_Init();

    /* Init Data */
    re_CratosMode = NORMAL;
    re_CratosAction = NO_ACTION;

    /* Load Thresholds from Eeprom */
    APP_CRATOS_THRESHOLD_INIT;
}

/* brief! This function runs the cratos functionality
 */
void app_Cratos_Task(void) {
    /* Display Task */
    app_Cratos_DisplayTask();
    /* Evaluate Thresholds */
    /* Relay Actions */
    rul_Data = app_HX711_GetMeasure();
    unsigned long lul_offset;
    lul_offset = app_Threshold_GetCalOffset();
    if(lul_offset > rul_Data)
    {
        lul_offset = rul_Data;
        app_Threshold_Calibrate(rul_Data);
    }
    rul_Data -= lul_offset;
    rul_Data *= app_Threshold_GetCalMultiplier();
    rul_Data /= 1000u;
    if (APP_CRATOS_EVALUATEMAX_MEAS(rul_Data)) {
        /* Measure greater than max threshold */
        APP_CRATOS_RELAY_ON;
    } else if (APP_CRATOS_EVALUATEMIN_MEAS(rul_Data)) {
        /* Measure lower than min threshold */
        APP_CRATOS_RELAY_OFF;
    } else {
        /* Relay Unchanged */
    }
}

/* brief! This function check the button inputs
 */
static void app_Cratos_DisplayTask(void) {
    switch (re_CratosMode) {
        default:
        case NORMAL:
        {
            /* Print normal Data */
            APP_CRATOS_PRINT_NORMAL_DATA;
        }break;
        case THRESHOLD_MAX:
        {
            /* Print threshold max Data */
            APP_CRATOS_PRINT_THRMAX_DATA;
        }break;
        case THRESHOLD_MIN:
        {
            /* Print threshold max Data */
            APP_CRATOS_PRINT_THRMIN_DATA;
        }
    }
}

/* brief! This function check the button inputs
 */
void app_Cratos_CheckInputs(void) {
    if (NO_ACTION != re_CratosAction) {

    } else {
        if ((APP_CRATOS_CHANGEMODE_STATE == APP_CRATOS_GET_BTN_STATE)) {
            /* Request Change mode action */
            re_CratosAction = CHANGE_MODE_ACTION;
        } else if (APP_CRATOS_UP_STATE == APP_CRATOS_GET_BTN_STATE) {
            /* Request Up Threshold */
            re_CratosAction = INCREASE_THRESHOLD;
        } else if (APP_CRATOS_FAST_UP_STATE == APP_CRATOS_GET_BTN_STATE) {
            /* Request Up Threshold */
            re_CratosAction = INCREASE_THRESHOLD;
        } else if (APP_CRATOS_DOWN_STATE == APP_CRATOS_GET_BTN_STATE) {
            /* Request Down Threshold */
            re_CratosAction = DECREASE_THRESHOLD;
        } else if (APP_CRATOS_FAST_DOWN_STATE == APP_CRATOS_GET_BTN_STATE) {
            /* Request Down Threshold */
            re_CratosAction = DECREASE_THRESHOLD;
        } else if (APP_CRATOS_CAL_STATE == APP_CRATOS_GET_BTN_STATE) {
            if(THRESHOLD_MAX == re_CratosMode)
            {
            /* Request Calibration */
            app_Threshold_Calibrate(app_HX711_GetMeasure());
            }
            else if(THRESHOLD_MIN == re_CratosMode)
            {
                /* Request Multiplier calculation */
            }
            else
            {
                /* Do nothing */
            }
        }else {
            /* No action needed */
            re_CratosAction = NO_ACTION;
        }
        app_BtnMngr_ClearNewDebBtnFlag();
    }
}

/* brief! This function execute the configuration actions requested
 */
void app_Cratos_CfgActions(void) {
    switch (re_CratosMode) {
        default:
        {
            re_CratosMode = NORMAL;
        }
        case NORMAL:
        {
            /* Check if Request is performed */
            if (CHANGE_MODE_ACTION == re_CratosAction) {
                re_CratosMode = THRESHOLD_MAX;
            } else {
                /* Ignore another request */
            }
            /* No action needed */
            re_CratosAction = NO_ACTION;
        }
            break;
        case THRESHOLD_MAX:
        {
            /* Check if Request is performed */
            if (CHANGE_MODE_ACTION == re_CratosAction) {
                re_CratosMode = THRESHOLD_MIN;
            } else if (INCREASE_THRESHOLD == re_CratosAction) {
                /* Increase max threshold */
                APP_CRATOS_INCREASE_THRESHOLD_MAX;
            } else if (DECREASE_THRESHOLD == re_CratosAction) {
                /* Decrease min threshold */
                APP_CRATOS_DECREASE_THRESHOLD_MAX;
            } else {
                /* No Action Requested */
            }
            /* No action needed */
            re_CratosAction = NO_ACTION;
        }
            break;
        case THRESHOLD_MIN:
        {
            /* Check if Request is performed */
            if (CHANGE_MODE_ACTION == re_CratosAction) {
                re_CratosMode = NORMAL;
            } else if (INCREASE_THRESHOLD == re_CratosAction) {
                /* Increase max threshold */
                APP_CRATOS_INCREASE_THRESHOLD_MIN;
            } else if (DECREASE_THRESHOLD == re_CratosAction) {
                /* Decrease min threshold */
                APP_CRATOS_DECREASE_THRESHOLD_MIN;
            } else {
                /* No Action Requested */
            }
            /* No action needed */
            re_CratosAction = NO_ACTION;
        }
            break;
    }
}
