/* 
 * File:   app_Relay.h
 * Author: uidj2522
 *
 * Created on December 10, 2019, 5:39 PM
 */

#ifndef APP_RELAY_H
#define	APP_RELAY_H

#ifdef	__cplusplus
extern "C" {
#endif
#include "../mcc_generated_files/pin_manager.h"
    
    /* Pin interface */
#define APP_RELAY_SET_RELAY_PIN     IO_RB0_SetHigh()
#define APP_RELAY_CLEAR_RELAY_PIN   IO_RB0_SetLow()
    
    /* Public  Functions */
extern void app_Relay_Init(void);
extern void app_Relay_On(void);
extern void app_Relay_Off(void);

#ifdef	__cplusplus
}
#endif

#endif	/* APP_RELAY_H */

