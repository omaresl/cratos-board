#include "app_Relay.h"

/* brief! This function inits the Relay Module
 */
void app_Relay_Init(void)
{
    /* Init Relay State */
    APP_RELAY_CLEAR_RELAY_PIN; //OFF
}

/* brief! This function turns on the relay
 */
void app_Relay_On(void)
{
    /* Turns On */
    APP_RELAY_SET_RELAY_PIN;
}

/* brief! This function turns off the relay
 */
void app_Relay_Off(void)
{
    /* Turns Off */
    APP_RELAY_CLEAR_RELAY_PIN;
}