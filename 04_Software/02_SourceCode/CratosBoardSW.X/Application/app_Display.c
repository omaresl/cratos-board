#include "app_Display.h"
#define N_DIGITS        5u
#define N_VALUES        3u

static unsigned int rub_Data = 0;
static char rub_Digits[N_VALUES][N_DIGITS + 1u] = {"0000 ","0000 ","0000 "};
/* brief! This function inits the display module
 */
void app_Display_Init(void)
{
    i2c_LCD_Init();
    i2c_LCD_SetBacklight();
    i2c_LCD_SetCursor(0,0);
}

/* brief! This function fills the display buffer with data relevant on normal mode
 */
void app_Display_Normal(void)
{
    char lub_Show[N_DIGITS];
    lub_Show[0] = rub_Digits[0][0];
    lub_Show[1] = rub_Digits[0][1];
    lub_Show[2] = 0u;
    /* First Line */
    i2c_LCD_SetCursor(0,0);
    i2c_LCD_Print("Peso: ");
    i2c_LCD_Print(lub_Show);
    i2c_LCD_Print(".");
    lub_Show[0] = rub_Digits[0][2];
    lub_Show[1] = rub_Digits[0][3];
    lub_Show[2] = rub_Digits[0][4];
    lub_Show[3] = 0u;
    i2c_LCD_Print(lub_Show);
    i2c_LCD_Print(" Kg ");
    /* Second Line */
    i2c_LCD_SetCursor(0,1);
    i2c_LCD_Print("                ");    
}

void app_Display_THRMAX(void)
{
    /* First Line */
    i2c_LCD_SetCursor(0,0);
    i2c_LCD_Print("Limite Superior ");
    i2c_LCD_SetCursor(0,1);    

    /* Second Line */
    i2c_LCD_Print(rub_Digits[2]);
    i2c_LCD_Print(" Kg             ");
}

void app_Display_THRMIN(void)
{
    /* First Line */
    i2c_LCD_SetCursor(0,0);
    i2c_LCD_Print("Limite Inferior ");
    i2c_LCD_SetCursor(0,1);    

    /* Second Line */
    i2c_LCD_Print(rub_Digits[1]);
    i2c_LCD_Print(" Kg             ");
    
}

/* brief! This function fills the display buffer with data relevant on normal mode
 */
void app_Display_SetData(unsigned int lub_Data,unsigned char lub_Index)
{
    i2c_LCD_Dec2Dig(lub_Data,N_DIGITS,rub_Digits[lub_Index]);
    i2c_LCD_Dig2ASCII((unsigned char*)rub_Digits[lub_Index],rub_Digits[lub_Index],N_DIGITS);
}