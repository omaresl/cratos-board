#include <pic18.h>

#include "app_Threshold.h"

__EEPROM_DATA(0x00,0x00,0x03,0xE8,0x00,0x00,0x03,0xDE);
__EEPROM_DATA(0x00,0x02,0x46,0x08,0x00,0x00,0x00,0x0A);
unsigned long rul_ThresholdMax = 0x00000000;
unsigned long rul_ThresholdMin = 0x00000000;
unsigned long rul_CalOffset = 0x00000000;
unsigned long rul_CalMultiplier = 0x00000000;

/* brief! This function inits the Thresholds
 */
void app_Threshold_Init(void)
{
    /* Load From EEprom Max Data */
    rul_ThresholdMax = \
            ((unsigned long)DATAEE_ReadByte(0x00) << 24u) |
            ((unsigned long)DATAEE_ReadByte(0x01) << 16u) |
            ((unsigned long)DATAEE_ReadByte(0x02) << 8u) |
            ((unsigned long)DATAEE_ReadByte(0x03));
    
    rul_ThresholdMin = \
            ((unsigned long)DATAEE_ReadByte(0x04) << 24u) |
            ((unsigned long)DATAEE_ReadByte(0x05) << 16u) |
            ((unsigned long)DATAEE_ReadByte(0x06) << 8u) |
            ((unsigned long)DATAEE_ReadByte(0x07));
    rul_CalOffset = \
            ((unsigned long)DATAEE_ReadByte(0x08) << 24u) |
            ((unsigned long)DATAEE_ReadByte(0x09) << 16u) |
            ((unsigned long)DATAEE_ReadByte(0x0A) << 8u) |
            ((unsigned long)DATAEE_ReadByte(0x0B));
    
    rul_CalMultiplier = \
            ((unsigned long)DATAEE_ReadByte(0x0C) << 24u) |
            ((unsigned long)DATAEE_ReadByte(0x0D) << 16u) |
            ((unsigned long)DATAEE_ReadByte(0x0E) << 8u) |
            ((unsigned long)DATAEE_ReadByte(0x0F));
}

/* brief! This function evaluates measure against thresholds
 */
unsigned char app_Threshold_EvaluateMax(unsigned long lul_Measure)
{
    unsigned char lub_Return;
    
    /* Init Return Variable */
    lub_Return = 0x00;
    if(lul_Measure >= rul_ThresholdMax)
    {
        lub_Return = 0x01;
    }
    else
    {
        lub_Return = 0x00;
    }
    return lub_Return;
}

/* brief! This function evaluates measure against thresholds
 */
unsigned char app_Threshold_EvaluateMin(unsigned long lul_Measure)
{
    unsigned char lub_Return;
    
    /* Init Return Variable */
    lub_Return = 0x00;
    if(lul_Measure <= rul_ThresholdMin)
    {
        lub_Return = 0x01;
    }
    else
    {
        lub_Return = 0x00;
    }
    return lub_Return;
}

/* brief! This function increases max threshold
 */
void app_Threshold_IncreaseMax(void)
{
    /* Check if max value has been reached */
    if(rul_ThresholdMax >= APP_THRESHOLD_MAX_VALUE)
    {
        rul_ThresholdMax = rul_ThresholdMin;
    }
    else
    {
        rul_ThresholdMax++;
    }
    DATAEE_WriteByte(0,(rul_ThresholdMax >> 24u));
    DATAEE_WriteByte(1,(rul_ThresholdMax >> 16u));
    DATAEE_WriteByte(2,(rul_ThresholdMax >> 8u));
    DATAEE_WriteByte(3,(rul_ThresholdMax >> 0u));
}

/* brief! This function incrases min threshold
 */
void app_Threshold_IncreaseMin(void)
{
    /* Check if max value has been reached */
    if(rul_ThresholdMin >= rul_ThresholdMax)
    {
        rul_ThresholdMin = APP_THRESHOLD_MIN_VALUE;
    }
    else
    {
        rul_ThresholdMin++;
    }
    DATAEE_WriteByte(4,(rul_ThresholdMin >> 24u));
    DATAEE_WriteByte(5,(rul_ThresholdMin >> 16u));
    DATAEE_WriteByte(6,(rul_ThresholdMin >> 8u));
    DATAEE_WriteByte(7,(rul_ThresholdMin >> 0u));
}

/* brief! This function decrease max threshold
 */
void app_Threshold_DecreaseMax(void)
{
    /* Check if max value has been reached */
    if(rul_ThresholdMax <= rul_ThresholdMin)
    {
        rul_ThresholdMax = APP_THRESHOLD_MAX_VALUE;
    }
    else
    {
        rul_ThresholdMax--;
    }
    DATAEE_WriteByte(0,(rul_ThresholdMin >> 24u));
    DATAEE_WriteByte(1,(rul_ThresholdMin >> 16u));
    DATAEE_WriteByte(2,(rul_ThresholdMin >> 8u));
    DATAEE_WriteByte(3,(rul_ThresholdMin >> 0u));
}

/* brief! This function decrease min threshold
 */
void app_Threshold_DecreaseMin(void)
{
    /* Check if max value has been reached */
    if(rul_ThresholdMin <= APP_THRESHOLD_MIN_VALUE)
    {
        rul_ThresholdMin = rul_ThresholdMax;
    }
    else
    {
        rul_ThresholdMin--;
    }
    DATAEE_WriteByte(4,(rul_ThresholdMin >> 24u));
    DATAEE_WriteByte(5,(rul_ThresholdMin >> 16u));
    DATAEE_WriteByte(6,(rul_ThresholdMin >> 8u));
    DATAEE_WriteByte(7,(rul_ThresholdMin >> 0u));
}

void app_Threshold_Calibrate(unsigned long lul_Cal)
{
    rul_CalOffset = lul_Cal + 100u;
    DATAEE_WriteByte(0x08,(rul_CalOffset >> 24u));
    DATAEE_WriteByte(0x09,(rul_CalOffset >> 16u));
    DATAEE_WriteByte(0x0A,(rul_CalOffset >> 8u));
    DATAEE_WriteByte(0x0B,(rul_CalOffset >> 0u));
}

unsigned long app_Threshold_GetMaxValue(void)
{
    return rul_ThresholdMax;
}
unsigned long app_Threshold_GetMinValue(void)
{
    return rul_ThresholdMin;
}

unsigned long app_Threshold_GetCalOffset(void)
{
    return rul_CalOffset;
}
unsigned long app_Threshold_GetCalMultiplier(void)
{
    return rul_CalMultiplier;
}
